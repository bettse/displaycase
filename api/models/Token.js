/* globals module, require */
/**
* Token.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    type: {
      type: 'string',
      enum: ["none", "trapmaster", "trap", "item", "location", "mini", "regular", "giant", "legendary"],
      defaultsTo: 'none'
    },
    element: {
      type: 'string',
      enum: ["none", "magic", "earth", "water", "fire", "tech", "undead", "life", "air", "dark", "light"],
      defaultsTo: 'none'
    },
    data: {
      type: 'text'
    }
  },

  seedData: require('../../seed.json')


};

