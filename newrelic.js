/* global exports, process */
//http://willricketts.com/using-newrelic-with-sails-js/
exports.config = {
  rules: {
    ignore : ['^/socket.io/*/xhr-polling']
  },
  app_name : ['displaycase'],
  license_key : process.env.NEW_RELIC_LICENSE_KEY,
  logging : { level : 'warn' }
};
